<?php

namespace App\Http\Controllers\Open;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class AdminsLoginController extends Controller
{

    public function login(Request $request){
        $validated = $this->validate($request, [
            'email' => ['required', 'string', 'email',],
            'password' => ['required', 'string', 'min:6',],
        ]);
        if($validated){
    	   return $this->loggin_check($request) ? redirect()->route('admin.dashboard') : redirect()->back()->with('msg','Invalid email or password');
        }
        // dd($request->all());
    }

    public function loggin_check($request){
    	$data = $request->only('email','password');
        $remember = $request->remember ? true : false;
    	$login_attempt = Auth::guard('admin')->attempt($data,$remember);
    	return $login_attempt ? true : false; 

    }
    public function logout(Request $request){
    	Auth::guard('admin')->logout();
        return redirect('/');
    }
}
