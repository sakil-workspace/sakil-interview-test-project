<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        for($i = 1; $i <= 50; $i++){
            
            App\Customer::create(
            	[
    	        'name' => $faker->name,
    	        'email' => $faker->unique()->safeEmail,
    	        'password' => bcrypt(123456),
                'status' => 1,
    	        'remember_token' => Str::random(10)
    	    	]
        	);
        }

    }
}
