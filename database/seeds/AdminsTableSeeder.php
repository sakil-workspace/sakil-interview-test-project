<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;
use Illuminate\Support\Str;
class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        $model = App\Admin::create(
        	[
	        'name' => $faker->name,
	        'email' => $faker->unique()->safeEmail,
	        'password' => bcrypt(123456)
	    	]
    	);

    }
}
