<?php


Route::get('/', function () {
    return view('app.login');
})->middleware('guest:admin')->name('home');
Route::namespace('Open')->group(function(){
	Route::get('register','CustomerRegisterController@register')->name('customer.register')->middleware('guest:admin');
	Route::post('register','CustomerRegisterController@create')->name('customer.create');
	Route::post('login','AdminsLoginController@login')->name('login');
	Route::get('/logout','AdminsLoginController@logout')->name('logout');
});
// ->middleware('auth:admin')
Route::namespace('Backend')->prefix('admin')->name('admin.')->middleware('auth:admin')->group(function(){

	Route::get('/','DashboardController@index')->name('dashboard');
	Route::post('/dashboard_customer_details','DashboardController@dashboard_customer_details')->name('dashboard_customer_details');
	Route::post('/dashboard_customer_delete','DashboardController@dashboard_customer_delete')->name('dashboard_customer_delete');
	Route::post('/dashboard_customer_destroy','DashboardController@dashboard_customer_destroy')->name('dashboard_customer_destroy');
	Route::post('/dashboard_customer_disable','DashboardController@dashboard_customer_disable')->name('dashboard_customer_disable');
	Route::post('/dashboard_customer_disabled','DashboardController@dashboard_customer_disabled')->name('dashboard_customer_disabled');
	
	



});

// Auth::routes();