<?php
namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;

class DashboardController extends Controller {    

    /** main dashboard **/
    public function index() {
        $result = Customer::orderBy('id','desc')->paginate(5); 
        return view('app.dashboard', compact('result'));
    }

    /** customer details **/

    public function dashboard_customer_details(Request $request){
        $model = Customer::findOrFail($request->id);
        return view('app.dashboard_customer_details', compact('model'));
    }
    /** customer delete modal **/

    public function dashboard_customer_delete(Request $request){
        $model = Customer::findOrFail($request->id);
        return view('app.dashboard_customer_delete', compact('model'));
    }

    /** customer destroy **/

    public function dashboard_customer_destroy(Request $request){
        Customer::destroy($request->id);
        return response()->json(['success' => true]);
    }
    /** customer disable **/

    public function dashboard_customer_disable(Request $request){
        $model = Customer::findOrFail($request->id);
        return view('app.dashboard_customer_disable', compact('model'));
    }
    
    /** customer disabled **/

    public function dashboard_customer_disabled(Request $request){
        $model = Customer::findOrFail($request->id);
        if($model->status == 1){
            $model->update(['status' => 0]);
        }else{
            $model->update(['status' => 1]);            
        }
        return response()->json(['success' => true]);
    }
    


}
