<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated {

    
    public function handle($request, Closure $next, $guard = null) {
        // dump($guard);
        if (Auth::guard($guard)->check()) {
            if($guard == 'admin'){
                return redirect('/admin');
            }
        }

        return $next($request);
    }

}
