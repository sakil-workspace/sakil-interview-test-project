<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    
    public function handle($request, Closure $next, $guard = null) {
        $is_logged_in = Auth::guard($guard)->check();
        if (!$is_logged_in) {
            return redirect('/');
        }
        return $next($request);
    }
}
