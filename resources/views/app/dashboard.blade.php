@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header"><h2>{{ 'Admin Dashboard' }}</h2></div>
                <div class="card-body">
                    <div class="alert alert-success">
                      <h3 class="text-center">Our valuable customers</h3>
                    </div>
                    @isset($result)
                    <table class="table table-striped table-sm table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th width="5%">ID</th>
                                <th width="25%">Name</th>
                                <th width="25%">Email</th>
                                <th width="10%" class="text-center">Status</th>
                                <th width="35%" class="text-center">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($result as $model)
                            <tr>
                                <td>{{ $model->id }}</td>
                                <td>{{ $model->name }}</td>
                                <td>{{ $model->email }}</td>
                                <td class="text-center">
                                    @if($model->status != 0)
                                    <span class="badge badge-success">{{ $model->status()[$model->status] }}</span>
                                    @else
                                    <span class="badge badge-secondary">{{ $model->status()[$model->status] }}</span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <button type="button" class="btn btn-success btn-sm btn_view" data-id="{{$model->id}}">View</button>
                                    {{-- <button type="button" class="btn btn-primary btn-sm btn_edit" data-id="{{$model->id}}">Edit</button> --}}
                                    <button type="button" class="btn btn-danger btn-sm btn_delete" data-id="{{$model->id}}">Delete</button>
                                    <button type="button" class="btn btn-warning btn-sm btn_disable" data-id="{{$model->id}}">
                                        @if($model->status == 0)
                                        Active
                                        @else
                                        Deactive
                                        @endif

                                    </button>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                    {{ $result->links() }}
                    @else
                    Nothing found
                    @endisset
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(document).ready(function(){

        /* disable ajax request */

        {

            $('.btn_disable').click(function(e){
                const ID = $(this).data('id');
                const data_object = {
                  URL : "{{ route('admin.dashboard_customer_disable') }}",
                  DATA : { id: ID},
                  DATATYPE : "",
                  METHOD : 'post',
                };
                custom_ajax_call(data_object);                

            });   
        }

        /* End: disable ajax request */

        /* disabled ajax request */

        {

        $('body').on('click','.btn_disabled',function(e){
            const ID = $(this).data('id');
            const data_object = {
              URL : "{{ route('admin.dashboard_customer_disabled') }}",
              DATA : { id: ID},
              DATATYPE : "json",
              NOMODAL : true,
              METHOD : 'post',
              RELOAD : true,
            };
            custom_ajax_call(data_object);              

          });   
        }
        /* End: disabled ajax request */

        /* view ajax request */
        {

            $('.btn_view').click(function(e){
                let id = $(this).data('id');
                const URL = "{{ route('admin.dashboard_customer_details') }}";
                const DATA = { id: id};
                const DATATYPE = "html";
                const METHOD = "post";
                custom_ajax_call({URL,DATA,DATATYPE,METHOD});                

            });   
        }

        /* End: view ajax request */

        /* delete ajax request */

        {

            $('.btn_delete').click(function(e){
                let id = $(this).data('id');
                const URL = "{{ route('admin.dashboard_customer_delete') }}";
                const DATA = { id: id};
                const DATATYPE = "html";
                const METHOD = "post";
                custom_ajax_call({URL,DATA,DATATYPE,METHOD});                

            });   
        }

        /* End: delete ajax request */

        /* destroy ajax request */

        {

        $('body').on('click','.btn_destroy',function(e){
            const ID = $(this).data('id');
            const data_object = {
              URL : "{{ route('admin.dashboard_customer_destroy') }}",
              DATA : { id: ID},
              DATATYPE : "json",
              NOMODAL : true,
              METHOD : 'post',
              RELOAD : true,
            };
            custom_ajax_call(data_object);              

          });   
        }
        /* End: destroy ajax request */


    });
</script>
@endpush