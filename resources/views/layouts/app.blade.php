<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Scripts -->
        
        
        <script src="{{ asset('js/app.js') }}"></script>
        <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        </script>
        <!-- Re-Capcha Script -->
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
                <div class="container">
                    @guest
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ env('APP_NAME', 'Laravel') }}
                    </a>
                    @else
                    <a class="navbar-brand" href="{{ route('admin.dashboard') }}">
                        {{ env('APP_NAME', 'Laravel') }}
                    </a>
                    @endguest
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">
                        </ul>
                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('home') }}">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('customer.register') }}">Register</a>
                            </li>
                            @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} ({{ Auth::user()->email }}) <span class="caret"></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}">
                                        Logout
                                    </a>
                                </div>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
            <main class="py-4">
                @yield('content')
            </main>
        </div>
        <!-- Push scripts dynamically -->
        
        <script>            

            /* Custom ajax call function */
            function custom_ajax_call({URL,DATA,DATATYPE,METHOD,NOMODAL,RELOAD}){
                $.ajax({
                    url: URL,
                    data: DATA,
                    dataType: DATATYPE,
                    type : METHOD,
                    success: function(result,status,xhr){
                        if(NOMODAL){
                            $('.modal').modal('hide');
                        }else{
                            $('body').append(result);
                            $('.modal').modal('show');
                        }
                        if(RELOAD){
                            location.reload();
                        }


                    },
                    error: function(xhr){
                        alert("An error occured: " + xhr.status + " " + xhr.statusText);
                    }
                });
            }
            /* End: Custom ajax call function */
            
            $(document).ready(function(){
                /* Hide all modal after modal hidden event */
                $('body').on('hidden.bs.modal',function(){
                    $('.modal').remove();
                });
                /* End: Hide all modal after modal hidden event */

            });
        </script>
        @stack('scripts')
    </body>
</html>