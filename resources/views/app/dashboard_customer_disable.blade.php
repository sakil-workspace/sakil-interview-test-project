<div class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <!-- Modal Header -->
      <div class="modal-header  text-center">
        <h4 class="modal-title">Customer Details</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
      </div>
      <form method="POST" action="">
        @csrf
        @method('post')
        <!-- Modal body -->
        <div class="modal-body">
          <p><strong>Name: </strong> {{ $model->name }}</p>
          <p><strong>Email: </strong> {{ $model->email }}</p>
          <p><strong>Status: </strong>
            @if($model->status != 0)
            <span class="badge badge-success">{{ $model->status()[$model->status] }}</span>
            @else
            <span class="badge badge-secondary">{{ $model->status()[$model->status] }}</span>
            @endif
          </p>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer text-center">
          <h4>Are you sure?
          <button type="button" class="btn btn-danger btn_disabled" data-id="{{ $model->id }}">Yes</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
          </h4>
        </div>
      </form>
      
    </div>
  </div>
</div>