<?php
namespace App\Http\Controllers\Open;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer;

class CustomerRegisterController extends Controller
{
    public function register(){
    	return view('app.register');
    }

    public function create(Request $request)
    {
    	$data = $request->except('_token','_method');
	    $validated = $this->validate($request, [
	        'name' => ['required', 'string', 'max:300'],
	        'email' => ['required', 'string', 'email', 'max:100', 'unique:customers'],
	        'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
        if($validated){
	        $created = Customer::create([
	            'name' => $request['name'],
	            'email' => $request['email'],
	            'password' => bcrypt($request['password']),
	        ]);
	        if($created){
	        	return redirect()->back()->with('msg', true);
	        }	
        }
    }


}
